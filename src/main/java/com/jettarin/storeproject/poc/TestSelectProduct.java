/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jettarin.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author NITRO 5
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
        
        try {
            String sql ="SELECT id, name, price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result =stmt.executeQuery(sql);
            while (result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
               double price = result.getDouble("price");
               Product product = new Product(id,name,price);
                System.out.println(product);
            }
          } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
    }
}
