/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.jettarin.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.User;

/**
 *
 * @author NITRO 5
 */
public class UserDao implements DaoInterface<User> {
     
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO User (name,tel,password) VALUES (?, ?,? );";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getName());
            stmt.setString(3, object.getTel());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

   
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name, tel, password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

               int id = result.getInt("id");
                String name = result.getString("name");
                String tel= result.getString("tel");
                 String password = result.getString("password");
                 
               User user = new User(id ,name, tel,password);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }

    
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name, tel, password FROM user WHERE id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

               int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(pid, name, tel, password);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

   
    public int delete(int id) {
        Connection conn = null;
            Database db = Database.getInstance();
            conn = db.getConnection();
             int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1,id);
            row =stmt.executeUpdate();
           
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
            db.close();
            return row;
    }

   
    public int update(User object) {
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
          int row = 0;

        try {
            String sql = "UPDATE user SET id = 'id',name = 'name',tel = 'tel',password = 'password' WHERE id = 1;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,object.getId());
            stmt.setString(2,object.getName());
             stmt.setString(3,object.getTel());
            stmt.setString(4,object.getPassword());
            row =stmt.executeUpdate();
            
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao Udao = new UserDao();        
      System.out.println(Udao.add(new User(-1,"Junpen","092901555","password")));
       System.out.println(Udao.getAll());
     System.out.println(Udao.get(1));
       
        
        
    }
}
